CREATE SCHEMA IF NOT EXISTS `employee` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;

CREATE TABLE `employee`.`employee` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  `position` VARCHAR(255) NOT NULL,
  `department` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)); 