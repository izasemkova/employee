package com.testproject.employee.dao;

import com.testproject.employee.entity.Employee;
import com.testproject.employee.entity.EmployeeMapper;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * This class contains methods that bind the Employee entity with database
 *
 * @version 1.0 26 March 2016
 * @author Irina Zasemkova
 */
public class EmployeeDaoImpl implements EmployeeDao {

    private DataSource dataSource;
    private NamedParameterJdbcTemplate jdbcTemplateObject;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new NamedParameterJdbcTemplate(dataSource);
    }

    /**
     * This method gets list of all employees from database
     *
     * @return list of employee
     */
    @Override
    public List<Employee> getEmployeeList() {
        List<Employee> employeeList = jdbcTemplateObject.query("select * from employee", new EmployeeMapper());
        return employeeList;
    }

    /**
     * This method gets list of all employees which satisfy search conditions.
     *
     * @param name employee name
     * @param lastname employee last name
     * @return list of employee
     */
    @Override
    public List<Employee> getFiltredEmployeeList(String name, String lastname) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", "%" + name + "%");
        params.addValue("lastname", "%" + lastname + "%");
        List<Employee> employeeList = jdbcTemplateObject.query("select * from employee where name like :name and lastname like :lastname", params, new EmployeeMapper());

        return employeeList;
    }

    /**
     * This method adds employee to database.
     *
     * @param employee employee that will be added
     * @return a result of adding new record to database
     */
    @Override
    public boolean addEmployee(Employee employee) {
        boolean isCreated = false;

        BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(employee);
        int createResult = jdbcTemplateObject.update("insert into employee (name,  lastname, position, department) values(:name,  :lastname, :position, :department)", params);
        isCreated = (createResult == 1);

        return isCreated;
    }

    /**
     * This method deletes employee with given id from database.
     *
     * @param id id of employee that need to be deleted
     * @return a result of deleting record with given id from database
     */
    @Override
    public boolean deleteEmployee(int id) {
        boolean isDeleted = false;
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);

        int deleteResult = jdbcTemplateObject.update("delete from employee where id = :id", params);
        isDeleted = (deleteResult == 1);

        return isDeleted;
    }

}
