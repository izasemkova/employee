package com.testproject.employee.dao;

import com.testproject.employee.entity.Employee;
import java.util.List;

public interface EmployeeDao {

    public List<Employee> getEmployeeList();

    public List<Employee> getFiltredEmployeeList(String name, String lastname);

    public boolean addEmployee(Employee employee);

    public boolean deleteEmployee(int id);

}
