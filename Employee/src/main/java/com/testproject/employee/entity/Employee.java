package com.testproject.employee.entity;

import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 * This class is entity that defines the employee's parameters.
 * 
 * @version 1.0 26 March 2016
 * @author Irina Zasemkova
 */
public class Employee {

    int id;
    @NotNull(message = "Пожалуйста, введите имя.")
    String name;
    @NotNull(message = "Пожалуйста, введите фамилию.")
    String lastname;
    @NotNull(message = "Пожалуйста, введите должность.")
    String position;
    @NotNull(message = "Пожалуйста, введите отдел.")
    String department;

    public Employee() {
    }

    public Employee(String name, String lastname, String position, String department) {
        this.name = name;
        this.lastname = lastname;
        this.position = position;
        this.department = department;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", lastname=" + lastname + ", position=" + position + ", department=" + department + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + this.id;
        hash = 29 * hash + Objects.hashCode(this.name);
        hash = 29 * hash + Objects.hashCode(this.lastname);
        hash = 29 * hash + Objects.hashCode(this.position);
        hash = 29 * hash + Objects.hashCode(this.department);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employee other = (Employee) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.lastname, other.lastname)) {
            return false;
        }
        if (!Objects.equals(this.position, other.position)) {
            return false;
        }
        if (!Objects.equals(this.department, other.department)) {
            return false;
        }
        return true;
    }

}
