package com.testproject.employee.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * @version 1.0 26 March 2016
 * @author Irina Zasemkova
 */
public class EmployeeMapper implements RowMapper<Employee> {

    @Override
    public Employee mapRow(ResultSet rs, int i) throws SQLException {
        Employee employee = new Employee();
        employee.setId(rs.getInt("id"));
        employee.setName(rs.getString("name"));
        employee.setLastname(rs.getString("lastname"));
        employee.setPosition(rs.getString("position"));
        employee.setDepartment(rs.getString("department"));

        return employee;

    }

}
