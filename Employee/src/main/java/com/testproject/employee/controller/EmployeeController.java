package com.testproject.employee.controller;

import com.testproject.employee.dao.EmployeeDaoImpl;
import com.testproject.employee.entity.Employee;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * This class is a controller which contains methods for handling CRUD
 * operations for Employee.
 *
 * @version 1.0 26 March 2016
 * @author Irina Zasemkova
 */
@Controller
public class EmployeeController {

    private final static Logger LOGGER = Logger.getLogger(EmployeeController.class);

    @Autowired
    EmployeeDaoImpl employeeDao;

    /**
     * This method handles request by adding a new employee. If entered values
     * are not valid it adds error message and displays it to user.
     *
     * @param newEmployee new employee entity which should be added to database
     * @param result BindingResult object
     * @param model
     * @return ModelAndView object
     */
    @RequestMapping(value = {"/add"}, method = RequestMethod.POST)
    public ModelAndView addEmployee(@Valid @ModelAttribute("newEmployee") Employee newEmployee, BindingResult result, Map<String, Object> model) {

        String errorMessage = "";

        ModelAndView mv = new ModelAndView("index");

        if (result.hasErrors()) {
            return mv;
        }
        if (!isValid(newEmployee)) {
            errorMessage = "Ошибка добавления сотрудника. Слишком длинное значение в одном из полей.\n Попробуйте еще раз";
            LOGGER.info("Aadding new employee error. One of values too long.");
            addEmployeeList(mv);
            mv.addObject("errorMessage", errorMessage);
            return mv;
        }

        boolean isCreated = employeeDao.addEmployee(newEmployee);
        if (!isCreated) {
            errorMessage = "Ошибка добавления сотрудника. \n Попробуйте еще раз";
            mv.addObject("errorMessage", errorMessage);
            LOGGER.debug("Adding new record to database error");
        } else {
            LOGGER.info("DATABASE CHANGED! New employee has been added");
        }

        addEmployeeList(mv);
        newEmployee = new Employee();
        model.put("newEmployee", newEmployee);

        addEmployeeList(mv);

        return mv;
    }

    /**
     * This method handles request by deleting an employee with given id.
     *
     * @param id id of employee who should be deleted from database
     * @param model
     * @return ModelAndView object
     */
    @RequestMapping(value = {"/delete"}, method = RequestMethod.GET)
    public ModelAndView addEmployee(@RequestParam(value = "id") int id, Map<String, Object> model) {

        String errorMessage = "";

        ModelAndView mv = new ModelAndView("index");

        boolean isDeleted = employeeDao.deleteEmployee(id);
        if (!isDeleted) {
            errorMessage = "Ошибка удаления сотрудника. \n Попробуйте еще раз";
            LOGGER.debug("Deleting record from database error");
        } else {
            LOGGER.info("DATABASE CHANGED! Employee with id " + id + "has been deleteed");
        }
        Employee newEmployee = new Employee();
        model.put("newEmployee", newEmployee);
        mv.addObject("errorMessage", errorMessage);

        addEmployeeList(mv);

        return mv;
    }

    /**
     * This method handles request by searching of employees with given name and
     * last name. If entered values are not valid it adds error message and
     * displays it to user.
     *
     * @param name 
     * @param lastname
     * @param model
     * @return ModelAndView object
     */
    @RequestMapping(value = {"/search"}, method = RequestMethod.POST)
    public ModelAndView index(@RequestParam(value = "name", defaultValue = "") String name,
            @RequestParam(value = "lastname", defaultValue = "") String lastname,
            Map<String, Object> model) {

        String errorMessage = "";

        ModelAndView mv = new ModelAndView("index");

        if (!isValid(name, lastname)) {
            errorMessage = "Ошибка! Слишком длинное значение в одном из полей.\n Попробуйте еще раз";
            LOGGER.info("Searching error. One of values too long.");
            addEmployeeList(mv);
            mv.addObject("errorMessage", errorMessage);
            Employee newEmployee = new Employee();
            model.put("newEmployee", newEmployee);
            return mv;
        }

        List<Employee> employeeList = employeeDao.getFiltredEmployeeList(name, lastname);

        Employee newEmployee = new Employee();
        model.put("newEmployee", newEmployee);
        mv.addObject("employeeList", employeeList);

        return mv;
    }

    private void addEmployeeList(ModelAndView mv) {

        List<Employee> employeeList = employeeDao.getEmployeeList();
        mv.addObject("employeeList", employeeList);
    }

    private boolean isValid(Employee newEmployee) {

        if (newEmployee.getName().length() > 40) {
            return false;
        }
        if (newEmployee.getLastname().length() > 40) {
            return false;
        }
        if (newEmployee.getPosition().length() > 200) {
            return false;
        }
        if (newEmployee.getDepartment().length() > 200) {
            return false;
        }

        return true;
    }

    private boolean isValid(String name, String lastname) {

        if (name.length() > 40) {
            return false;
        }
        if (lastname.length() > 40) {
            return false;
        }

        return true;
    }

}
