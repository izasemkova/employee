package com.testproject.employee.controller;

import com.testproject.employee.dao.EmployeeDaoImpl;
import com.testproject.employee.entity.Employee;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.apache.log4j.Logger;

/**
 * This class is a main controller which contains method for handling a request
 * to index page.
 *
 * @version 1.0 26 March 2016
 * @author Irina Zasemkova
 */
@Controller
public class MainController {

    private final static Logger LOGGER = Logger.getLogger(MainController.class);

    @Autowired
    EmployeeDaoImpl employeeDao;

    @RequestMapping(value = {"/index", "/"}, method = RequestMethod.GET)
    public ModelAndView index(Map<String, Object> model) {

        ModelAndView mv = new ModelAndView("index");

        Employee newEmployee = new Employee();
        model.put("newEmployee", newEmployee);

        List<Employee> employeeList = employeeDao.getEmployeeList();

        mv.addObject("employeeList", employeeList);
        LOGGER.info("Index page was downloaded");
        return mv;
    }

}

