<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Список сотрудников</title>
        <link
            href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css"
            rel="stylesheet">

        <SCRIPT type="text/javascript">
<!--
            function validateForm() {
                valid = true;

                if (document.addEmployeeForm.name.value === "") {
                    alert("Пожалуйста заполните поле 'Имя'.");
                    valid = false;
                }
                if (document.addEmployeeForm.lastname.value === "") {
                    alert("Пожалуйста заполните поле 'Фамилия'.");
                    valid = false;
                }
                if (document.addEmployeeForm.position.value === "") {
                    alert("Пожалуйста заполните поле 'Должность'.");
                    valid = false;
                }
                if (document.addEmployeeForm.department.value === "") {
                    alert("Пожалуйста заполните поле 'Отдел'.");
                    valid = false;
                }
                return valid;
            }
//-->
        </SCRIPT>

    </head>
    <body>
        <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script
        src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>

        <div class="container">
            <div class="page-header">
                <h1>
                    Список сотрудников
                </h1>
            </div>
            <p style="color:red;">${errorMessage}</p>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Должность</th>
                        <th>Отдел</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${employeeList}" var="employee">
                        <tr>
                            <td>${employee.id}</td>
                            <td>${employee.name}</td>
                            <td>${employee.lastname}</td>  
                            <td>${employee.position}</td> 
                            <td>${employee.department}</td> 
                            <td><a href="delete.htm?id=${employee.id}">Удалить</a> </td> 
                        </tr>
                    </c:forEach>
                </tbody>
            </table>

            <div class="col-md-6">
                <div class="page-header">
                    <h4>
                        Добавление нового сотрудника
                    </h4>
                </div>
                <form:form name="addEmployeeForm" method="POST" action="add.htm"
                           class="form-horizontal" role="form" commandName="newEmployee" onsubmit="return validateForm();">
                    <div class="form-group">
                        <label for="name" class="control-label col-sm-2 "> Имя
                        </label>
                        <div class="col-sm-6">
                            <form:input type="text" path="name" class="form-control" id="name"
                                        name="name"/><br/> 
                            <form:errors style="color:red;" path="name" cssClass="error" ></form:errors> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label col-sm-2">Фамилия
                            </label>
                            <div class="col-sm-6">
                            <form:input type="text" path="lastname" class="form-control" id="lastname"
                                        name="lastname" /> <br/> 
                            <form:errors style="color:red;" path="lastname" cssClass="error" ></form:errors> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="position" class="control-label col-sm-2"> Должность
                            </label>
                            <div class="col-sm-6">
                            <form:input type="text" path="position" class="form-control" id="position"
                                        name="position" /><br/> 
                            <form:errors style="color:red;" path="position" cssClass="error" ></form:errors> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="department" class="control-label col-sm-2"> Отдел
                            </label>
                            <div class="col-sm-6">
                            <form:input type="text" path="department" class="form-control" id="department"
                                        name="department" /><br/> 
                            <form:errors style="color:red;" path="department" cssClass="error" ></form:errors> 
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-6">
                                <input type="submit" class="btn btn-success btn-block"
                                       value="Добавить сотрудника" />
                            </div>
                        </div>
                </form:form>
            </div>

            <div class="col-md-6">
                <div class="page-header">
                    <h4>
                        Поиск
                    </h4>
                </div>
                <form name="searchEmployee" method="POST" action="search.htm"
                      class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-2"> Имя
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" 
                                   name="name" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2"> Фамилия
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" 
                                   name="lastname" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-6">
                            <input type="submit" class="btn btn-success btn-block"
                                   value="Выбрать" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
